package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {
	
	private int objectId;
	private String location;
	private int totalPaid;
	private String ticketIssueDate;
	private String accidentIndicator;
	private String violationDescription;
	private String violatioCode;

	public VOMovingViolations(int pOjectId, String pLocation, int pTotalPaid, String pTicketIssueDate, String pAccidentIndicator, String pViolationDescription,String pViolatioCode )
	{
		objectId = pOjectId;
		location = pLocation;
		totalPaid = pTotalPaid;
		ticketIssueDate = pTicketIssueDate;
		accidentIndicator = pAccidentIndicator;
		violationDescription = pViolationDescription;
		violatioCode = pViolatioCode;
		
	}
	
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}
	
	public String getViolationCode() {
		
		return violatioCode;
	}
}
