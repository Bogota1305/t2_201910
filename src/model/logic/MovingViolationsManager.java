package model.logic;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

public class MovingViolationsManager implements IMovingViolationsManager {

	private LinkedList<VOMovingViolations> infracciones = new LinkedList<>();
	
	public void loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub
		
		String archivoCsv = "./data/Moving_Violations_Issued_in_January_2018.csv";
		
		try {
			CSVReader Csvr = new CSVReader(new FileReader(archivoCsv));
			String[] lector = new String[16];
	
			while((lector = Csvr.readNext()) != null)
			{
				lector = Csvr.readNext();
				
				VOMovingViolations infraccion = new VOMovingViolations(Integer.parseInt(lector[0]), lector[2], Integer.parseInt(lector[9]), lector[13], lector[12], lector[15], lector[14]);
				infracciones.add(infraccion);
				System.out.println(lector[0]);	
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

		
	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> retornar = new LinkedList<>();
		
		for(int i = 0; i< infracciones.size(); i++)
		{
			if(infracciones.get(i).getViolationCode().equals(violationCode))
			{
				retornar.add(infracciones.get(i));
			}
		}
		System.out.println(retornar);
		
		return retornar;
	}

	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> retornar = new LinkedList<>();
		
		
		for(int i = 0; i< infracciones.size(); i++)
		{
			if(infracciones.get(i).getAccidentIndicator().equals(accidentIndicator))
			{
				retornar.add(infracciones.get(i));
			}
		}
				
		System.out.println(retornar);
		
		return retornar;
	}	


}
